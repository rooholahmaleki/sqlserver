from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import auth

def login(request):
    args = {'message': False}
    if request.method == "POST":
        user = auth.authenticate(
            username=request.POST['username'],
            password=request.POST['password']
        )
        if user:
            auth.login(request, user)
            return HttpResponseRedirect('/user/users/')
        else:
            args = {'message': True}
            return render(request, 'login.html', args)
    else:
        return render(request, 'login.html', args)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')
