from class_singleton import Singleton
import pyodbc

@Singleton
class SqlConnection(object):
    __db = None

    @classmethod
    def get_connection(cls):
        if cls.__db is None:
            cnx = pyodbc.connect("DSN=SQLDemo;UID=mechani1_user;PWD=!Z11111111", autocommit=True)
            cls.__db = cnx.cursor()
        return cls.__db

    def __init__(self):
        self.get_connection()

cursor = SqlConnection().get_connection()
