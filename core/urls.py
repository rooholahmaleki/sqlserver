from core.views import *
from django.conf.urls import url

urlpatterns = [
    url(r'^$', login, name='login'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
]
