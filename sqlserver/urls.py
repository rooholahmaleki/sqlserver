from django.conf.urls import include, url
from core.views import *

urlpatterns = [
    url(r'^', include('core.urls')),
    url(r'^users/', include('users.urls')),
]