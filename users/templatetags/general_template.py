#! -*- coding: utf-8 -*-
# coding: utf-8

from __future__ import unicode_literals

from django import template
import jdatetime
import datetime

register = template.Library()

@register.filter(name='persian_date')
def persian_date(date):
    if isinstance(date, datetime.datetime) or isinstance(date, datetime.date):
        return_date = jdatetime.date.fromgregorian(
            day=date.day,
            month=date.month,
            year=date.year,
        )
        if date.hour == 0 and date.minute == 0:
            hour = ""
        else:
            hour = " - "+str(date.hour)+":"+str(date.minute)

        return str(return_date).replace('-', '/').split(' ')[0]+hour
    return 'وارد نشده است'