#! -*- coding: utf-8 -*-
# coding: utf-8
import jdatetime
from users.controller import *
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_GET, require_POST

@require_GET
def users(request):
    args = {
        'users': get_all_users()
    }
    return render(request, 'users.html', args)


@require_POST
def delete_user(request):
    cell_number = request.POST['cell_number']
    print delete_user_by_id(cell_number).rowcount
    return JsonResponse({})


def add_user(request):
    if request.method == "POST":
        data = {
            'CellNumber': request.POST['CellNumber'],
            'FName': u'{0}'.format(request.POST["FName"]).encode('utf-8'),
            'LName': u'{0}'.format(request.POST["LName"]).encode('utf-8'),
            # 'Photo': request.POST['Photo'],
            'BirthDate': str(convert_date_jalaji_to_gregorian(request.POST['BirthDate']))+'.123',
            'Username': request.POST['Username'],
            'Gender': request.POST['Gender'],
            'Fax': request.POST['Fax'],
            'Address': u'{0}'.format(request.POST['Address']).encode('utf-8'),
            'Password': request.POST['Password'],
            'Email': request.POST['Email'],
            'Phone': request.POST['Phone']
        }
        if request.POST['cell_number_edit'] == "":
            query = insert_user(data)
            message = "insert"
        else:
            data['CellNumber'] = request.POST['cell_number_edit']
            query = update_user(data)
            message = "update"

        if query:
            return JsonResponse({'message': message})

    else:
        return render(request, 'add_user.html')

def convert_date_jalaji_to_gregorian(date):
    date = date.split('/')
    end_date = jdatetime.JalaliToGregorian(int(date[0]), int(date[1]), int(date[2]))
    date = datetime.datetime(end_date.gyear, end_date.gmonth, end_date.gday)
    return date


def edit_user(request, cell_number):
    args = {
        'user': find_user(cell_number)
    }
    return render(request, 'add_user.html', args)
