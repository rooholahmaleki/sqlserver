from users.views import *
from django.conf.urls import url
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', users, name='users'),
    url(r'^delete/$', delete_user, name='delete_user'),
    url(r'^add/$', add_user, name='add_user'),
    url(r'^edit/(?P<cell_number>[\w\d]+)/$', edit_user, name='edit_user'),
]

