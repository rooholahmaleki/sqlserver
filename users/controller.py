#! -*- coding: utf-8 -*-
# coding: utf-8

import datetime
from core.db_conection import cursor


def get_all_users():
    cursor.execute("SELECT * FROM users;")
    columns = [column[0] for column in cursor.description]
    users = []
    for row in cursor.fetchall():
        users.append(dict(zip(columns, row)))

    return users


def delete_user_by_id(cell_number):
    string_sql = "delete from users where CellNumber='{0}';".format(cell_number)
    return cursor.execute(string_sql)


def insert_user(data):
    date = str(datetime.datetime.now()).split('.')[0].replace(' ', 'T')
    data['BirthDate'] = str(data['BirthDate']).split('.')[0].replace(' ', 'T')
    string_sql = "INSERT INTO " \
                 "users(" \
                 "CellNumber," \
                 "FName," \
                 "LName," \
                 "BirthDate," \
                 "Username," \
                 "Gender," \
                 "Fax," \
                 "ModifyDate," \
                 "CreateDate," \
                 "Address," \
                 "Password," \
                 "Email," \
                 "Phone)" \
                 "VALUES (" \
                 "'{0}', '{1}', '{2}', convert(DATETIME,'{3}'), '{4}', '{5}'," \
                 "'{6}', convert(DATETIME,'{7}'), convert(DATETIME,'{8}'), '{9}', '{10}', '{11}'," \
                 "'{12}');".format(data['CellNumber'],
                                   data['FName'],
                                   data['LName'],
                                   data['BirthDate'],
                                   data['Username'],
                                   data['Gender'],
                                   data['Fax'],
                                   date,
                                   date,
                                   data['Address'],
                                   data['Password'],
                                   data['Email'],
                                   data['Phone']
                                   )

    return cursor.execute(string_sql)


def find_user(cell_number):
    string_sql = "select * from users where CellNumber='{0}';".format(cell_number)
    cursor.execute(string_sql)
    columns = [column[0] for column in cursor.description]
    user = ""
    for row in cursor.fetchall():
        user = dict(zip(columns, row))

    return user


def update_user(data):
    date = str(datetime.datetime.now()).split('.')[0].replace(' ', 'T')
    data['BirthDate'] = str(data['BirthDate']).split('.')[0].replace(' ', 'T')
    string_sql = "UPDATE users " \
                 "SET " \
                 "FName='{1}'," \
                 "LName='{2}'," \
                 "BirthDate='{3}'," \
                 "Username='{4}'," \
                 "Gender='{5}'," \
                 "Fax='{6}'," \
                 "ModifyDate='{7}'," \
                 "Address='{8}'," \
                 "Password='{9}'," \
                 "Email='{10}'," \
                 "Phone='{11}' " \
                 "WHERE CellNumber='{0}';" \
        .format(
                data['CellNumber'],
                data['FName'],
                data['LName'],
                data['BirthDate'],
                data['Username'],
                data['Gender'],
                data['Fax'],
                date,
                data['Address'],
                data['Password'],
                data['Email'],
                data['Phone']
                )

    return cursor.execute(string_sql)
