from django.db import models
from django.contrib.auth.models import User as DjangoUser


class UserBox(models.Model):
    user = models.OneToOneField(DjangoUser, related_name='userBox')
    mongo_id = models.CharField(max_length=24)
    full_name = models.CharField(max_length=60)
    user_type = models.CharField(max_length=10)

    def __unicode__(self):
        return self.user.username
