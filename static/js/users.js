var csrf_token = "";
var option = "";

function delete_user(cell_number) {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {
        var data = {
            'cell_number': cell_number,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/users/delete/',
            data: data,
            success: function (resp) {
                $('#user_' + id_user).remove();
                uikitNotify('کاربر شما پاک شد.', '', 'success');
            },
            error: function (e) {
                uikitNotify('سیستم با مشکل روبه رو شده است.', '', 'danger');
                console.log(e);
            }
        });
    });

}

$('#add_user').submit(function (e) {
    e.preventDefault();
    if (check_password() && check_date($('#BirthDate').val().split('/'))){
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/users/add/',
            data: $(this).serializeArray(),
            success: function (resp) {
                if(resp['message'] == "insert"){
                    $('#add_user')[0].reset();
                    uikitNotify('کاربر شما با موفقیت اضافه شد', '', 'success');
                }else{
                    uikitNotify('به روزرسانی شد.', '', 'success');
                }
            },
            error: function (e) {
                uikitNotify('سیستم با مشکل روبه رو شده است.', '', 'danger');
            }
        });
    }
});


function check_password(){
    if ($('#Password').val() != $('#rep_password').val()){
        uikitNotify('پسووردها مطابقت ندارند', '', 'danger');
        return false;
    }
    return true;
}

function check_date(date) {
    try {
        var year = parseInt(date[0]),
            month = parseInt(date[1]),
            day = parseInt(date[2]);

        if (1300 < year && year < 1900 && 0 < month && month < 13 && 0 < day && day < 32) {
            return true
        }
        uikitNotify('تاریخ ها درست وارد نشده اند', '', 'danger');
        return false;

    } catch (e) {
        uikitNotify('تاریخ ها درست وارد نشده اند', '', 'danger');
        return false
    }
}