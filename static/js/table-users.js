var TableDatatablesResponsive = function () {
    var n = function () {
        var e = $("#users-table");
        e.dataTable({
            language: {
                aria: {
                    sortAscending: ": مرتب سازی صعودی",
                    sortDescending: ": مرتب سازی نزولی"
                },
                emptyTable: "هیچ کاربری وجود ندارد",
                info: " _START_ تا _END_ از _TOTAL_ کاربر",
                infoEmpty: "",
                infoFiltered: "",
                lengthMenu: "تعداد جهت نمایش :  _MENU_",
                search: "جستجو:",
                zeroRecords: "هیچ چیزی یافت نشد"
            },
            buttons: [
                {extend: "print", className: "btn dark btn-outline", text: "پرینت"},
                {extend: "excel", className: "btn blue btn-outline", text: "اکسل"},
                {extend: "colvis", className: "btn red btn-outline", text: "حذف ستون ها"}
            ],
            responsive: {details: {}},
            order: [[0, "asc"]],
            lengthMenu: [[5, 10, 20, 40, 80, -1], [5, 10, 20, 40, 80, "همه"]],
            pageLength: 10,
            dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
        })
    };
    return {
        init: function () {
            jQuery().dataTable && (n())
        }
    }
}();
jQuery(document).ready(function () {
    TableDatatablesResponsive.init()
});