function active_menu_item(selector_page_item) {
    $(selector_page_item).addClass('start');
    $(selector_page_item).addClass('active');
    $(selector_page_item).addClass('open');
}

function display_block_sub_menu(selector_page_item){
    $(selector_page_item).css('display', 'block')
}

function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}