//variables for keeping notifications skip and limit
var notifications_limit = 20;

var notifications_date = {
    post_most_recent: null,
    post_last_read: null,
    product_most_recent: null,
    product_last_read: null,
    friend_request_most_recent: null,
    friend_request_last_read: null
}


function get_more_notifications_from_notifications(type, method) {

    var notifications = null;
    marked_olds = mark_user_notifications_as_read();

    if(marked_olds) {
        var data = {
            'csrfmiddlewaretoken': getCookie('csrftoken'),
            'type': type,
            'method': method,
            'limit': notifications_limit
        }
        // check if type is ['like', 'mention', 'comment']
        if(typeof type === 'object') {
            type = 'post';
        }
        data['created_date'] = ((method === 'load_more') ? notifications_date[type + '_last_read'] : notifications_date[type + '_most_recent']),
        data = JSON.stringify(data);

        $.ajax({
            url: urls['get_user_notifications'], // the endpoint
            type: "POST", // http method
            async: false,
            cache: false,
            dataType: 'json',
            data: data,
            // handle a successful response
            success: function (response) {
                if(response['result'] === 'Success'){
                    notifications = response['message'];
                    if(notifications) {
                        if(method === 'load_more') {
                            notifications_date[type + '_last_read'] = notifications[notifications.length - 1]['created_date'];
                        }
                        else if(method === 'load_new') {
                            notifications_date[type + '_most_recent'] = notifications[0]['created_date'];
                        }
                    }
                }
                else {
                    console.log(response);
                }
            },
            // handle a non-successful response
            error: function (err) {
                console.log(err);
            }
        });
    }
    else {
        console.log('old notifications could not mark as read!');
    }
    return notifications;
}


function get_user_notifications_from_notifications(type) {

    var notifications = null;
    var data = {
        'csrfmiddlewaretoken': getCookie('csrftoken'),
        'limit': notifications_limit,
        'type': type
    };
    data = JSON.stringify(data);
    $.ajax({
        url: urls['get_user_notifications'], // the endpoint
        type: "POST", // http method
        async: false,
        cache: false,
        dataType: 'json',
        data: data,
        // handle a successful response
        success: function (response) {
            if(response['result'] === 'Success'){
                notifications = response['message'];

                if(notifications) {
                    if(typeof type === 'object') {
                        type = 'post';
                    }
                    notifications_date[type + '_most_recent'] = notifications[0]['created_date'];
                    notifications_date[type + '_last_read'] = notifications[notifications.length - 1]['created_date'];
                }
            }
            else {
                console.log(response);
            }
        },
        // handle a non-successful response
        error: function (err) {
            console.log(err);
        }
    });

    return notifications;
}


function load_notifications_from_notifications(main_type, method) {

    if(typeof method === 'undefined') {
        method = 'load_more';
    }
    var type = null;
    var parent_ul = null;
    var notifications = null;

    if(main_type === 'post') {
        type = ['like', 'mention', 'comment'];
        parent_ul = $('#id_post_notifications_ul');
    }
    else if(main_type === 'product') {
        type = main_type;
        parent_ul = $('#id_product_notifications_ul');
    }
    else if(main_type === 'friend_request') {
        type = main_type;
        parent_ul = $('#id_friend_request_notifications_ul');
    }

    if(parent_ul.children().length < 1) {
        notifications = get_user_notifications_from_notifications(type);
    }
    else {
        notifications = get_more_notifications_from_notifications(type, method);
    }

    if(notifications) {
        add_to_notifications_from_notifications(notifications, parent_ul, method);
    }
}


function add_to_notifications_from_notifications(notifications, parent_ul, method) {

    var tmp_n = null;
    if(method === 'load_more') {
        for (var i = 0; i < notifications.length; i++) {
            tmp_n = notifications[i];
            if(tmp_n['read'] === false) {
                notifications_ids.push(tmp_n['id']);
            }
            html_notify = create_html_notify_element_from_notificatons(tmp_n);
            parent_ul.append(html_notify);
        }
    }
    else if(method === 'load_new') {
        for (var i = notifications.length - 1; i > -1; i--) {
            tmp_n = notifications[i];
            if(tmp_n['read'] === false) {
                notifications_ids.push(tmp_n['id']);
            }
            html_notify = create_html_notify_element_from_notificatons(tmp_n);
            parent_ul.prepend(html_notify);
        }
    }
}


function create_html_notify_element_from_notificatons(tmp_n) {

    var html_notify = '';
    if(tmp_n['type'] === 'friend_request') {
        html_notify =
            '<li class="vir-request ' +
                ((tmp_n['read'] == true) ? 'read" ' : 'unread" ') +
                'id="id_notification_' + tmp_n['id'] + '_li"' +
            '>' +
                '<div class="uk-grid uk-grid-collapse uk-flex-middle">' +
                    '<a href="' + urls['users_profile'] + tmp_n['target_id'] + '">' +
                        '<img src="' + tmp_n['thumb_img'] + '"' +
                             'alt="' + tmp_n['first_name'] + ' ' + tmp_n['last_name'] + '"' +
                             'width="75" height="75">' +
                    '</a>' +
                    '<div id="id_notification_' + tmp_n['id'] + '_li_msgbox" class="vir-request-box uk-flex-middle">' +
                        '<p>درخواست دوستی ' + tmp_n['first_name'] + ' ' + tmp_n['last_name'] + ' را قبول می کنید؟</p>' +
                        '<a class="vir-btn-30 uk-button uk-float-left uk-margin-small-right"' +
                           'onclick="accept_friend_request_from_base(\'id_notification_' +
                                tmp_n['id'] + '_li\', \'id_' +
                                tmp_n['target_id'] + '_request_in_li\', \'' +
                                online_user['id'] + '\', \'' +
                                online_user['first_name'] + '\', \'' +
                                online_user['last_name'] + '\', \'' +
                                online_user['thumb_img'] + '\', \'' +
                                tmp_n['target_id'] + '\', \'' +
                                tmp_n['first_name'] + '\', \'' +
                                tmp_n['last_name'] + '\', \'' +
                                tmp_n['thumb_img'] +
                            '\')"' +
                        '>بله</a>' +
                        '<a class="vir-btn-31 uk-button uk-float-left"' +
                           'onclick="delete_friend_request_from_base(\'id_notification_' +
                                tmp_n['id'] + '_li\', \'id_' +
                                tmp_n['target_id'] + '_request_in_li\', ' +
                                '\'in\', \'' +
                                online_user['id'] + '\', \'' +
                                online_user['first_name'] + '\', \'' +
                                online_user['last_name'] + '\', \'' +
                                online_user['thumb_img'] + '\', \'' +
                                tmp_n['target_id'] + '\', \'' +
                                tmp_n['first_name'] + '\', \'' +
                                tmp_n['last_name'] + '\', \'' +
                                tmp_n['thumb_img'] +
                            '\')"' +
                        '>خیر</a>' +
                    '</div>' +
                '</div>' +
            '</li>';
    }
    else if(tmp_n['type'] === 'like') {
        html_notify =
            '<li class="vir-rate ' +
                ((tmp_n['read'] == true) ? 'read" ' : 'unread" ') +
                'id="id_notification_' + tmp_n['id'] + '_li" '+
            '>' +
                '<a href="' + urls['user_image'] + '?post_id=' + tmp_n['target_id'] + '">' +
                    '<i class="virgole-virgolefont-40"></i>' +
                    '<p>' +
                        tmp_n['first_name'] + ' ' +
                        tmp_n['last_name'] +
                        ' پست ارسالی شما را پسندید.' +
                    '</p>' +
                '</a>' +
            '</li>';

// <li class="vir-recommend" id="id_product_notification_li">
//     <a href="#" class="uk-grid uk-grid-collapse uk-flex-middle">
//         <p class="uk-flex-middle">گوشی آیفون 6 - 64 گیگ 25 هزار تومان کاهش پیدا کرده است.</p>
//         <img src="" alt="{{ user.first_name }} {{ user.last_name }}" width="75" height="75">
//     </a>
// </li>
    }

    return html_notify;
}


$( document ).ready(function() {
    var main_types = ['post', 'product', 'friend_request'];
    for (var i = 0; i < main_types.length; i++) {
        load_notifications_from_notifications(main_types[i]);
    }
    delete main_types;

    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });

});
